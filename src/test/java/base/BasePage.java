package base;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import java.time.Duration;
import java.util.List;

import static core.Log.log;

public abstract class BasePage {
    final String TAG = "    BasePage():                   | "; // 35 + |
    private int DEFAULT_WAIT_TIME = 10; //default look for elements
    private int FAST_WAIT_TIME = 5; //default look for elements

    protected WebDriver driver;

    public BasePage(WebDriver driver){
        this.driver = driver;
        setDefaultWaitTime();
    }

    // Init. elements and set DEFAULT_WAIT_TIME to wait for element
    public void setDefaultWaitTime(){
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, DEFAULT_WAIT_TIME), this);
    }

    // Init. elements and set FAST_WAIT_TIME to wait for element
    public void setFastWaitTime(){
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, FAST_WAIT_TIME), this);
    }

    // Init. elements and set specific time to wait for element
    public  void setWaitSeconds(int sec ){
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, sec), this);
    }

    //  provide a page unique element to verify page is loaded
    public boolean isPageLoaded(List<WebElement> element, int sec){
        log( TAG+"isPageLoad: ");
        if(sec==-1){
            setDefaultWaitTime();
        } else {setWaitSeconds(sec);
        }
        boolean isLoaded=!element.isEmpty();
        setDefaultWaitTime();
        return isLoaded;
    }


    public void enterText(List<WebElement> e, String keys) {
        e.get(0).sendKeys(keys);
    }

    public void enterText(WebElement e, String keys) {
        e.sendKeys(keys);
    }

    public boolean click(List<WebElement> elList, int num){
        return click(elList.get(num));
    }

    public boolean click(List<WebElement> elList){
        return click(elList.get(0));
    }

    public boolean click(WebElement el){

        boolean click = false;
        for (int i = 0; i < 5; i++){ // 5 attempts to click
            try{
                el.click();
                click = true;
                break;
            } catch (Exception ex){
                log(TAG + "click(): FAILED attempt "+i);
            }
        }
        return click;
    }

    private WebElement scrollToElement(WebElement el){
        JavascriptExecutor js = (JavascriptExecutor) driver;
        if (el != null) {
            js.executeScript("arguments[0].scrollIntoView();", el);
        } else {
            log(TAG+ "scrollToElement(): FAILED - element is NULL");
        }
        return el;
    }


    public WebElement scrollToElement(List<WebElement> el){
       return scrollToElement(el.get(0));
    }

    public boolean isDisplayed (List<WebElement> el){
        boolean bool;
        if(!el.isEmpty()){
            try {
                bool = el.get(0).isDisplayed();
            } catch (Exception ex){
                bool = false; }
        } else {
            bool = false;
        }
        return bool;
    }

    public void sleep(int msec) {
        try {
            Thread.sleep(msec);
        } catch (Exception e) {
            // ignore
        }
    }

}
