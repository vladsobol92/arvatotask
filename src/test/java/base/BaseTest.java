package base;

import core.Arvato;
import core.driverFactory.DriverFactory;
import core.driverFactory.DriverType;
import org.aspectj.util.FileUtil;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;

import static core.Log.*;
public class BaseTest {

    public String homePageLink = "https://itarvato.ee";

    public WebDriver driver;
    public SoftAssert softAssert;
    public Arvato arvato;

    @BeforeMethod(alwaysRun = true)
    @Parameters({"browser"})
    public void beforeMethod(@Optional("CHROME") String browser){
        log("      beforeMethod():");
        DriverType browserType = getBrowserType(browser);
        driver = new DriverFactory().getDriver(browserType); // init Webdriver according to provided parameters
        softAssert = new SoftAssert();
        arvato = new Arvato(driver);
        driver.get(homePageLink);
        driver.manage().window().fullscreen();

    }

    @AfterMethod(alwaysRun = true)
    public void afterMethod(ITestResult result, ITestContext context, Method m){
        log("      afterMethod():");
        float runtime = (result.getEndMillis() - result.getStartMillis())/1000f; // time of test run
        log("EXECUTION TIME : "+runtime +" sec.");
        if (!result.isSuccess()){
            takeScreenShot(m);  // take screenshot if testResult != SUCCESS
            log("TEST: '"+m.getName()+"' FAILED");
        } else {
            log("TEST: '"+m.getName()+"' PASSED");
        }
        if (driver != null){
            log("      quitDriver():");
            driver.quit();
        }
    }

    private DriverType getBrowserType(String browser){
        log("   getBrowserType(): "+browser);
        try {
            return DriverType.valueOf(browser);
        } catch (Exception ex) {
            log("   getBrowserType(): FAILED - NO browser with type " + browser);
            log("   getBrowserType(): Starting default CHROME driver...");
            return DriverType.CHROME;
        }
    }

    // Screenshot taker
    // screenshots will be saved  to target/screenshots/$(testMethodName + timestamp).png/
    private void takeScreenShot(Method method){
        try {
            File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE.FILE);
            String name = (method.getName()+System.currentTimeMillis()).concat(".png");
            FileUtil.copyFile(scrFile, new File("target/screenshots/"+name));
        }catch (Exception ex){
            log(        "takeScreenShot(): FAILED");
        }
    }

}
