package pages;

import base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.List;
import static core.Log.log;
public abstract class HeaderMenu extends BasePage {

    final String TAG = "    HeaderMenu():                 | "; // 35 + |

    @FindBy(css = "[id=menu-item-118]")
    private List<WebElement> careerMenu;

    @FindBy(css = "[id=menu-item-119]")
    private List<WebElement> itDevelopmentCenterMenu;

    @FindBy(css = "[id=menu-item-120]")
    private List<WebElement> contactUsMenu;

     HeaderMenu(WebDriver driver) {
        super(driver);
    }

    public CareerPage navigateToCareer(){
         log(TAG + "navigateToCareer()");
        Assert.assertTrue(
                click(careerMenu),"navigateToCareer(): FAILED"
        );
        return new CareerPage(driver);
    }


}
