package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.testng.Assert;

import java.io.File;
import java.util.List;
import static core.Log.log;
public class JobPage extends HeaderMenu{

    final String TAG = "    JobPage():                    | "; // 35 + |

    private String resumeFile = new File("src/test/java/files/resume.pdf").getAbsolutePath();
    private String coverLetterFile = new File("src/test/java/files/coverLetter.pdf").getAbsolutePath();

    @FindBy(css = "a[href='#apply-form']")
    private List<WebElement> applyNow;

    @FindBy(css = "[class=wpcf7-form]")
    private List <WebElement> applicationForm;

    @FindBy(css = "[id=first_name]")
    private List <WebElement> firstNameInput;

    @FindBys(value = {
            @FindBy(css = "[class=first-input]"),
            @FindBy(css = "[class=wpcf7-not-valid-tip]")
    })
    public List <WebElement> firstNameInputErrorMsg;

    @FindBy(css = "[id=lat_name]")
    private List <WebElement> lastNameInput;

    @FindBys(value = {
            @FindBy(css = "[class*=apply-last-name]"),
            @FindBy(css = "[class*=wpcf7-not-valid-tip]")
    })
    public List <WebElement> lastNameInputErrorMsg;

    @FindBy(css = "[name=apply-email]")
    private List <WebElement> emailInput;

    @FindBys(value = {
            @FindBy(css = "[class*=apply-email]"),
            @FindBy(css = "[class*=wpcf7-not-valid-tip]")
    })
    public List <WebElement> emailInputErrorMsg;

    @FindBy(css = "[id=resume]")
    private List <WebElement> resumeAttach;

    @FindBy(css = "[id=coverLetter]")
    private List <WebElement> coverLetterAttach;

    @FindBy(css = "[id=file-upload-resume]")
    private List <WebElement> resumeUploadedMsg;

    @FindBy(css = "[id=file-upload-cover]")
    private List <WebElement> coverLetterUploadedMsg;

    @FindBy(css = "[class=submit-apply]")
    private List <WebElement> applyButton;

    @FindBy(css = "div[class*=wpcf7-mail-sent-ok")
    private List <WebElement> messageSubmitSuccess;

    @FindBy(css = "div[class*=wpcf7-validation-errors")
    private List <WebElement> messageSubmitError;


    public JobPage(WebDriver driver) {
        super(driver);
    }

    public boolean isPageLoaded(){
        return isPageLoaded(applyNow,-1);
    }

    public JobPage navigateToForm(){
      log(TAG+"navigateToForm()");
      scrollToElement(applicationForm);
      return this;
    }

    public JobPage setApplicantDetails(String name, String lastName, String email){
        log(TAG+"setApplicatnDetails()");
        enterText(firstNameInput, name);
        enterText(lastNameInput,lastName);
        enterText(emailInput,email);
        return this;
    }

    public JobPage attachResume(){
        log(TAG+"clickAttachResume()");
        enterText(resumeAttach,resumeFile);
        return this;
    }

    public JobPage attachCoverLetter(){
        log(TAG+"attachCoverLetter()");
        enterText(coverLetterAttach,coverLetterFile);
        return this;
    }

    public JobPage clickApplyButton(){
        log(TAG+"clickApplyButton()");
        Assert.assertTrue(click(applyButton),"clickApplyButton(): FAILED");
        return this;
    }


    public boolean isResumeUploaded(){
        log(TAG+"isResumeUploaded()");
        String resumeUplMsg =  resumeUploadedMsg.get(0).getText();
        String filename = new File(resumeFile).getName();

        return resumeUplMsg.contains(filename);
    }

    public boolean isCoverLetterUploaded(){
        log(TAG+"isCoverLetterUploaded()");
        String coverLetterUplMsg = coverLetterUploadedMsg.get(0).getText();
        String filename = new File(coverLetterFile).getName();
        return coverLetterUplMsg.contains(filename);
    }

    public boolean isSubmitSuccess(boolean success){
        log(TAG+"isSubmitSuccess(): "+success);
        boolean bool;
        setFastWaitTime();
        if (success) {
            bool = isDisplayed(messageSubmitSuccess);
        } else {
            bool = isDisplayed(messageSubmitError);
        }
        setDefaultWaitTime();
        return bool;
    }


}
