package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.List;
import static core.Log.log;
public class CareerPage extends HeaderMenu {

    final String TAG = "    CareerPage():                 | "; // 35 + |
    
    @FindBy(css = "[class=job-name")
    List<WebElement> jobs;

    public CareerPage(WebDriver driver) {
        super(driver);
    }

    public boolean isPageLoaded(){
        return isPageLoaded(jobs,-1);
    }

    public JobPage openJob(int number){
        log(TAG+"openJob(): "+number);
        Assert.assertTrue(click(jobs.get(number-1)),"");
        return new JobPage(driver);
    }
}
