package core.driverFactory;

public enum DriverType {

    CHROME, FIREFOX
}
