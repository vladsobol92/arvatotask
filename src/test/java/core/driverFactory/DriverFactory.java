package core.driverFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import static core.Log.log;
public class DriverFactory {

    final String TAG = "    DriverFactory():                | "; // 35 + |

    public WebDriver getDriver (DriverType type){
        log(TAG + "getDriver(): "+type);
        WebDriver webDriver = null;
        for (int i = 0; i < 5; i++) {
            try {
                switch (type) {
                    case CHROME:
                        System.setProperty("webdriver.chrome.driver","./src/test/resources/chromedriver");
                        webDriver = new ChromeDriver();
                        break;
                    case FIREFOX:
                        System.setProperty("webdriver.gecko.driver","./src/test/resources/geckodriver");
                        System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE,"/dev/null"); // workaround to remove Firefox logs
                        webDriver = new FirefoxDriver();
                        break;
                    default:
                        webDriver = new ChromeDriver();
                }
            } catch (Exception ex) {
                log("____________________________________");
                log(TAG+"Driver creation failed after: " + i +" attempt. Retrying...");
                log(ex.getMessage());
            }
            if (webDriver!=null) { // break cycle if WebDiver is successfully created
                break;
            }
        }
        if (webDriver == null) {
            throw new RuntimeException("Driver Creation failed");
        }

        return webDriver;
    }

}
