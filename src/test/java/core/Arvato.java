package core;

import org.openqa.selenium.WebDriver;
import pages.CareerPage;
import pages.HomePage;
import pages.JobPage;

public class Arvato {

    WebDriver driver;

    public Arvato (WebDriver driver){
        this.driver = driver;
    }

    public HomePage homePage(){
        return new HomePage(driver);
    }

    public CareerPage careerPage(){
        return new CareerPage(driver);
    }

    public JobPage jobPage(){
        return new JobPage(driver);
    }

}
