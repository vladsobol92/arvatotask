package tests.jobTests;

import base.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;

public class JobTestNegative extends BaseTest {

    String invalidEmail = "testTest.com";

    @Test(description = "Submit empty form")
    public void submitEmptyJobForm(){
        arvato.homePage()
                .navigateToCareer().openJob(1)
                .navigateToForm()
                .clickApplyButton();

        softAssert.assertTrue(
        arvato.jobPage().isDisplayed(
                arvato.jobPage().firstNameInputErrorMsg
        ),"Validation message for 'firstName' input is NOT shown");

        softAssert.assertTrue(
                arvato.jobPage().isDisplayed(
                        arvato.jobPage().lastNameInputErrorMsg
                ),"Validation message for 'lastName' input is NOT shown");

        softAssert.assertTrue(
                arvato.jobPage().isDisplayed(
                        arvato.jobPage().emailInputErrorMsg
                ),"Validation message for 'email' input is NOT shown");


        softAssert.assertTrue(arvato.jobPage().isSubmitSuccess(false),
                "Form submit 'Failure' message is NOT displayed");
        softAssert.assertAll();
    }


    @Test(description = "Submit form wit invalid email")
    public void submitFormWithInvalidEmail(){

        arvato.homePage()
                .navigateToCareer().openJob(1)
                .navigateToForm()
                .setApplicantDetails("Test","TestLname",invalidEmail)
                .attachResume()
                .attachCoverLetter();

        softAssert.assertTrue(arvato.jobPage().isResumeUploaded(), "Resume is NOT uploaded");
        softAssert.assertTrue(arvato.jobPage().isCoverLetterUploaded(), "Cover Letter is NOT uploaded");
        /*
       arvato.jobPage().clickApplyButton();
        softAssert.assertTrue(arvato.jobPage().isSubmitSuccess(false),"Form is NOT submitted");
        softAssert.assertTrue(
                arvato.jobPage().isDisplayed(
                        arvato.jobPage().emailInputErrorMsg
                ),"Validation message for 'email' input is NOT shown");
        */
        softAssert.assertAll();
    }

    // Broken test.
    // Implemented just to show framework behaviour with FAILED tests
    @Test(description = "This test is broken")
    public void brokenTest(){
        arvato.homePage()
                .navigateToCareer().openJob(1)
                .navigateToForm()
                .setApplicantDetails("Test","TestLname",invalidEmail)
                .clickApplyButton();

        // Dummy assertion to fail test
        Assert.assertTrue(false, "Dummy assertion to show test failure");
    }



}
