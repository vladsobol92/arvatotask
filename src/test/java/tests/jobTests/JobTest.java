package tests.jobTests;

import base.BaseTest;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.JobPage;
;

public class JobTest extends BaseTest{

    String firstName = RandomStringUtils.randomAlphabetic(6);
    String lastName =  RandomStringUtils.randomAlphabetic(6);
    String email = RandomStringUtils.randomAlphabetic(6).concat("@gmail.com");


    @Test(description = "Apply for job with full data, Resume and cover letter")
    public void applyForJobWithCVAndCoverLetter(){

        navigateToJob();
        arvato.jobPage()
                .navigateToForm()
                .setApplicantDetails(firstName,lastName,email)
                .attachResume();
        softAssert.assertTrue(arvato.jobPage().isResumeUploaded(), "Resume is NOT uploaded");

        arvato.jobPage()
               .attachCoverLetter();
        softAssert.assertTrue(arvato.jobPage().isCoverLetterUploaded(), "Cover Letter is NOT uploaded");

       // arvato.jobPage().clickApplyButton();
        softAssert.assertTrue(arvato.jobPage().isSubmitSuccess(true),"Form is NOT submitted");
        softAssert.assertAll();
    }


    @Test(description = "Apply for job with full data and only Resume")
    public void applyForJobWithResume(){

        navigateToJob();
        arvato.jobPage()
                .navigateToForm()
                .setApplicantDetails(firstName,lastName,email)
                .attachResume();
        softAssert.assertTrue(arvato.jobPage().isResumeUploaded(), "Resume is NOT uploaded");

     //   arvato.jobPage().clickApplyButton();
        softAssert.assertTrue(arvato.jobPage().isSubmitSuccess(true),"Form is NOT submitted");
        softAssert.assertAll();
    }


    @Test(description = "Apply for job with full data and only cover letter")
    public void applyForJobWithCoverLetter(){

        navigateToJob();
        arvato.jobPage()
                .navigateToForm()
                .setApplicantDetails(firstName,lastName,email);

        arvato.jobPage()
                .attachCoverLetter();
        softAssert.assertTrue(arvato.jobPage().isCoverLetterUploaded(), "Cover Letter is NOT uploaded");

     //   arvato.jobPage().clickApplyButton();
        softAssert.assertTrue(arvato.jobPage().isSubmitSuccess(true),"Form is NOT submitted");
        softAssert.assertAll();
    }


    // Method navigates to 'Job' page
    // Assertions are implemented to stop the test if navigation to 'Job' page failed, so no need to continue test.
    private void navigateToJob(){
        arvato.homePage()
                .navigateToCareer();
        Assert.assertTrue(arvato.careerPage().isPageLoaded(),"'Career' page is NOT open");
        arvato.careerPage().openJob(1); // open first job in the list
        Assert.assertTrue(arvato.jobPage().isPageLoaded(), "'Job' page is NOT OPEN" );
    }

}
